////////////////////////////////////////////////////////////////////////////////
// side-bar.js                                                                //
// A Vue app to render the board and let the player visually interact with it //
// Experts: Jonathan, Muhan, Ninnie, Alexey                                   //
////////////////////////////////////////////////////////////////////////////////

// When the user clicks anywhere outside of the popup, close it
window.onclick = function(event) {
	if (event.target == $("#achievementPopup")[0]) {
	  CloseAchievement();
	}
  }

function OpenAchievement(){
	$("#achievementPopup").css("display", "block");
}

function CloseAchievement(){
	$("#achievementPopup").css("display", "none");
}

let app = new Vue({
	el: '#side-bar',

	components: {
		VSwatches: window['vue-swatches']
	},

	data: {
		// The instance of Painter used throughout the app
		painter: null,
		// A string representing latest tile hashes received from backend, or
		// null if no hashes received yet
		tileHashes: null,
		// The board, stored as a 2D array of zipped values
		board: [],
		// Updates to board that haven't been confirmed by the server
		sneakPeeks: [],
		// Object that helps to cleanly resolve race conditions
		// waiting is true if app just sent an update and is waiting for a
		// response. num is the number of responses to updates received. oldNum
		// is the value of num at the time of the scheduled update request.
		race: {waiting: false, num: 0, oldNum: 0},
		// Coordinates of the selected pixel
		selectXY: {x: 0, y: 0},
		// AP progress - only used by addAP()
		progress: {fraction: 0, lastUpdate: NaN},
		color: SWATCHES[0],
		AP: 0,
		totalAP: 0,
		APSpent: 0,
		colorsChanged: 0,
		messagesSent: 0,
		colorCrazy: 0,
		pixellationNation: 0,
		spendCrazy: 0,
		APAce: 0,
		chatterbox: 0,
		swatches: SWATCHES,
		// Array of chat messages
		chats: [],
		// New chat
		newChat: {nick: "", message: ""},
		message: "temp"
	},

	computed: {
		pixel: function () {
			if (!this.board.length) return {
				x: this.selectXY.x,
				y: this.selectXY.y,
				colorText: "Loading",
				colorCSS: "#000",
				color: 0,
				lockText: "Loading",
				locked: false,
				numLocks: 0
			};
			let sneak = this.sneakPeeks.find(s => (s.x === this.selectXY.x
							&& s.y === this.selectXY.y));
			let zipped = (sneak !== undefined) ? sneak.value
							: this.board[this.selectXY.x][this.selectXY.y];
			let color = zipped % COLORS.length;
			let numLocks = Math.floor(zipped / COLORS.length);
			return {
				x: this.selectXY.x,
				y: this.selectXY.y,
				colorText: COLORS[color].name,
				colorCSS: COLORS[color].css,
				color: color,
				lockText: numLocks.toString(),
				locked: numLocks > 0,
				numLocks: numLocks
			};
		},

		actionLimits: function () {
			let canLock = true, canUnlock = true, canChange = true;
			let canTalk = true;
			let errorLock = "", errorUnlock = "", errorChange = "";
			let errorTalk = "";
			if (this.AP < COST_LOCK) {
				canLock = false;
				errorLock = "Not enough AP";
			}
			if (this.pixel.numLocks === 0) {
				canUnlock = false;
				errorUnlock = "Pixel has no locks";
			} else if (this.AP < COST_UNLOCK) {
				canUnlock = false;
				errorUnlock = "Not enough AP";
			}
			if (this.pixel.numLocks > 0) {
				canChange = false;
				errorChange = "Pixel is locked";
			} else if (this.AP < COST_CHANGE) {
				canChange = false;
				errorChange = "Not enough AP";
			}
			if (this.newChat.message.trim().length === 0) {
				canTalk = false;
				errorTalk = "Message is empty";
			} else if (this.AP < COST_TALK) {
				canTalk = false;
				errorTalk = "Not enough AP";
			} else if (this.newChat.nick.trim().length > MAX_NICK
							|| this.newChat.message.trim().length > MAX_MSG) {
				errorTalk = "Nick and / or message is too long and will be"
								+ " trimmed";
			}
			return {
				canLock: canLock,
				canUnlock: canUnlock,
				canChange: canChange,
				canTalk: canTalk,
				errorLock: errorLock,
				errorUnlock: errorUnlock,
				errorChange: errorChange,
				errorTalk: errorTalk
			}
		}
	},

	methods: {
		getMessage: function () {
			this.race.oldNum = this.race.num;
			fetch(BOARD_URL, {
				headers: {"Tile-Hashes": this.tileHashes}
			})
				.then(response => response.json())
				.then(json => {
					if (this.race.num > this.race.oldNum) return;
					if (!this.race.waiting) this.loadBoard(json);
				})
				.catch(error => console.log(
					`GET board/ error: ${JSON.stringify(error)}`
				));
		},

		getChats: function () {
			fetch(CHAT_URL)
				.then(this.loadChats)
				.catch(error => console.log(
					`GET chat/ error: ${JSON.stringify(error)}`
				));
		},

		loadBoard: function (tiles) {
			this.updateTileHashes(tiles);

			let firstTime = (this.board.length === 0);
			// Doing some extra copying to make sure Vue notices the change
			let oldBoard = this.board;
			let changes = [];
			this.board = Array(WIDTH);
			let receivedBoard = this.getReceivedBoard(tiles);
			for (let x = 0; x < WIDTH; ++x) {
				this.board[x] = Array(HEIGHT);
				for (let y = 0; y < HEIGHT; ++y) {
					let receivedValue = receivedBoard[x][y];
					if (receivedValue === null) {
						// No value provided by the server, our existing one
						// should be good
						if (firstTime) console.error("no value oh no");
						this.board[x][y] = oldBoard[x][y];
						continue;
					}
					if (!firstTime && oldBoard[x][y] !== receivedValue) {
						changes.push({x: x, y: y});
					}
					this.board[x][y] = receivedValue;
				}
			}
			this.painter.loadBoard(this.board, changes);
			this.sneakPeeks = [];
		},

		updateTileHashes: function (tiles) {
			let nextTileHashesCollector = [];
			for (let i = 0; i < NUM_TILES * NUM_TILES; ++i) {
				if (tiles[i] !== null) {
					nextTileHashesCollector.push(tiles[i].slice(
						0, HASH_LENGTH
					));
				} else {
					if (this.tileHashes === null) {
						console.warn(
							"tile hashes really should be initialized here"
						);
						nextTileHashesCollector.push(".".repeat(HASH_LENGTH));
					} else {
						nextTileHashesCollector.push(this.tileHashes.slice(
							i * HASH_LENGTH, (i + 1) * HASH_LENGTH
						));
					}
				}
			}
			this.tileHashes = nextTileHashesCollector.join("");
		},

		getReceivedBoard: function (tiles) {
			// Initialize receivedBoard to a correct-size array
			let receivedBoard = Array(WIDTH);
			for (let x = 0; x < WIDTH; ++x) {
				receivedBoard[x] = Array(HEIGHT);
				for (let y = 0; y < HEIGHT; ++y) {
					receivedBoard[x][y] = null;
				}
			}

			for (let tileX = 0; tileX < NUM_TILES; ++tileX) {
				for (let tileY = 0; tileY < NUM_TILES; ++tileY) {
					if (tiles[tileX + tileY * NUM_TILES] === null) continue;
					let tileZip = tiles[tileX + tileY * NUM_TILES]
						.slice(HASH_LENGTH);

					let pointer = 0;

					function readChar() {
						console.assert(pointer < tileZip.length);
						char = tileZip[pointer];
						++pointer;
						console.assert(BASE_64_ALPHABET.indexOf(char) > -1);
						return BASE_64_ALPHABET.indexOf(char);
					}

					for (let y = 0; y < TILE_SIZE; ++y) {
						for (let x = 0; x < TILE_SIZE; ++x) {
							let char0 = readChar();
							let singleNumber;
							if(char0 < 32) {
								singleNumber = char0;
							} else {
								let char1 = readChar();
								singleNumber = 64 * (char0 - 32) + char1;
							}
							receivedBoard
								[tileX * TILE_SIZE + x]
								[tileY * TILE_SIZE + y]
								= singleNumber;
						}
					}

					console.assert(pointer === tileZip.length);
				}
			}

			return receivedBoard;
		},

		loadChats: async function (response) {
			let elChats = document.getElementById("chats");
			let oldMaxScroll = elChats.scrollHeight - elChats.clientHeight;

			let chatsZip = await response.json();
			this.chats = chatsZip.map(chatZip => ({
				nick: chatZip.n,
				message: chatZip.m
			}));

			// Code adapted from: https://bit.ly/2Qz1sNE
			for (let chat of this.chats) {
				let hash = 0;
				for (let i = 0; i < chat.nick.length; ++i) {
					let c = chat.nick.charCodeAt(i);
					hash = 31 * hash + c;
					hash = hash % 360;
				}
				chat.color = "hsl(" + hash + ", 100%, 80%)";
			}

			setTimeout(() => {
				let newMaxScroll = elChats.scrollHeight - elChats.clientHeight;
				elChats.scrollTop += newMaxScroll - oldMaxScroll;
			}, 0);
		},

		addAP: function (timestamp) {
			let elapsed = isNaN(this.progress.lastUpdate) ? 0
							: timestamp - this.progress.lastUpdate;
			let timeToComplete = PROGRESS_BASE
							* Math.pow(PROGRESS_RATIO, this.AP);
			this.progress.fraction += elapsed / timeToComplete;
			if (this.progress.fraction >= 1) {
				this.progress.fraction = 0;
				this.AP += 1;
				this.changeTotalAP(1);
			}
			this.progress.lastUpdate = timestamp;
			window.requestAnimationFrame(this.addAP);
		},

		getAP: function () {
			if (localStorage.AP) {
				this.AP = parseInt(localStorage.AP);
			}
		},

		getAchievementCounters: function () {
			if (localStorage.totalAP) {
				this.totalAP = parseInt(localStorage.totalAP);
			}
			if (localStorage.APSpent) {
				this.APSpent = parseInt(localStorage.APSpent);
			}
			if (localStorage.colorsChanged) {
				this.colorsChanged = parseInt(localStorage.colorsChanged);
			}
			if (localStorage.messagesSent) {
				this.messagesSent = parseInt(localStorage.messagesSent);
			}
		},

		getAchievements: function () {
			if (localStorage.colorCrazy) {
				this.colorCrazy = parseInt(localStorage.colorCrazy);
				for (let i = 1; i <= this.colorCrazy; i++) {
					$("#CC" + i.toString()).css("opacity", 1);
				}
			}
			if (localStorage.pixellationNation) {
				this.pixellationNation = parseInt(localStorage.pixellationNation);
				if (this.pixellationNation == 1) {
					$("#PN1").css("opacity", 1);
				}
				else if (this.pixellationNation == 2) {
					$("#PN2").css("opacity", 1);
				}
				else if (this.pixellationNation == 3) {
					$("#PN1").css("opacity", 1);
					$("#PN2").css("opacity", 1);
				}
				else if (this.pixellationNation == 4) {
					$("#PN3").css("opacity", 1);
				}
				else if (this.pixellationNation == 5) {
					$("#PN1").css("opacity", 1);
					$("#PN3").css("opacity", 1);
				}
				else if (this.pixellationNation == 6) {
					$("#PN2").css("opacity", 1);
					$("#PN3").css("opacity", 1);
				}
				else if (this.pixellationNation == 7) {
					$("#PN1").css("opacity", 1);
					$("#PN2").css("opacity", 1);
					$("#PN3").css("opacity", 1);
				}
			}
			if (localStorage.spendCrazy) {
				this.spendCrazy = parseInt(localStorage.spendCrazy);
				for (let i = 1; i <= this.spendCrazy; i++) {
					$("#SC" + i.toString()).css("opacity", 1);
				}
			}
			if (localStorage.APAce) {
				this.APAce = parseInt(localStorage.APAce);
				for (let i = 1; i <= this.APAce; i++) {
					$("#AP" + i.toString()).css("opacity", 1);
				}
			}
			if (localStorage.chatterbox) {
				this.chatterbox = parseInt(localStorage.chatterbox);
				for (let i = 1; i <= this.chatterbox; i++) {
					$("#CB" + i.toString()).css("opacity", 1);
				}
			}
		},

		sendLock: function () {
			if (this.AP < COST_LOCK) return;
			this.AP -= COST_LOCK;
			fetch(BOARD_URL, {
				method: "PATCH",
				headers: {
					"Content-Type": "application/json",
					"Tile-Hashes": this.tileHashes
				},
				body: JSON.stringify({
					x: this.pixel.x,
					y: this.pixel.y,
					currColor: this.pixel.color,
					action: "LOCK"
				})
			})
				.then(response => response.json())
				.then((json) => {
					this.race.waiting = false;
					++this.race.num;
					this.loadBoard(json.tiles);
					if (!json.success){
						this.AP += COST_LOCK;
						return;
					}
					this.spendAP(COST_LOCK);
				})
				.catch(error => console.log(error));
			this.sneakPeeks.unshift({
				x: this.pixel.x,
				y: this.pixel.y,
				value: (this.pixel.numLocks + 1) * COLORS.length
								+ this.pixel.color
			});
			this.race.waiting = true;
		},

		sendUnlock: function () {
			if (this.pixel.numLocks === 0 || this.AP < COST_UNLOCK) return;
			this.AP -= COST_UNLOCK;
			fetch(BOARD_URL, {
				method: "PATCH",
				headers: {
					"Content-Type": "application/json",
					"Tile-Hashes": this.tileHashes
				},
				body: JSON.stringify({
					x: this.pixel.x,
					y: this.pixel.y,
					currColor: this.pixel.color,
					action: "UNLOCK"
				})
			})
				.then(response => response.json())
				.then((json) => {
					this.race.waiting = false;
					++this.race.num;
					this.loadBoard(json.tiles);
					if (!json.success){
						this.AP += COST_UNLOCK;
						return;
					}
					this.spendAP(COST_UNLOCK);
				})
				.catch(error => console.log(error));
			this.sneakPeeks.unshift({
				x: this.pixel.x,
				y: this.pixel.y,
				value: (this.pixel.numLocks - 1) * COLORS.length
								+ this.pixel.color
			});
			this.race.waiting = true;
		},

		sendChange: function () {
			if (this.pixel.numLocks > 0 || this.AP < COST_CHANGE) return;
			let nextColor = 0;
			for (i = 0; i < SWATCHES.length; i++) {
				if (SWATCHES[i] == this.color) {
					nextColor = i;
				}
			}
			if (nextColor == this.pixel.color) return;

			this.AP -= COST_CHANGE;
			fetch(BOARD_URL, {
				method: "PATCH",
				headers: {
					"Content-Type": "application/json",
					"Tile-Hashes": this.tileHashes
				},
				body: JSON.stringify({
					x: this.pixel.x,
					y: this.pixel.y,
					currColor: this.pixel.color,
					action: "CHANGE",
					nextColor: nextColor
				})
			})
				.then(response => response.json())
				.then((function (x, y, json) {
					this.race.waiting = false;
					++this.race.num;
					this.loadBoard(json.tiles);
					if (!json.success){
						this.AP += COST_CHANGE;
						return;
					}
					this.spendAP(COST_CHANGE);
					this.colorsChanged += 1;
					if (this.colorsChanged >= 1000 && this.colorCrazy < 5) {
						this.colorCrazy = 5;
						this.AP += 250;
						this.changeTotalAP(250);
						this.message = "Color Crazy V";
					}
					else if (this.colorsChanged >= 500 && this.colorCrazy < 4) {
						this.colorCrazy = 4;
						this.AP += 100;
						this.changeTotalAP(100);
						this.message = "Color Crazy IV";
					}
					else if (this.colorsChanged >= 250 && this.colorCrazy < 3) {
						this.colorCrazy = 3;
						this.AP += 50;
						this.changeTotalAP(50);
						this.message = "Color Crazy III";
					}
					else if (this.colorsChanged >= 50 && this.colorCrazy < 2) {
						this.colorCrazy = 2;
						this.AP += 10;
						this.changeTotalAP(10);
						this.message = "Color Crazy II";
					}
					else if (this.colorsChanged >= 10 && this.colorCrazy < 1) {
						this.colorCrazy = 1;
						this.AP += 1;
						this.changeTotalAP(1);
						this.message = "Color Crazy I";
					}
					if (this.pixellationNation % 2 === 0 && this.checkRow(y)) {
						this.pixellationNation += 1;
						this.AP += 50;
						this.changeTotalAP(50);
						this.message = "Pixellation Nation I";
					}
					if (this.pixellationNation < 2 && this.checkCol(x)) {
						this.pixellationNation += 2;
						this.AP += 50;
						this.changeTotalAP(50);
						this.message = "Pixellation Nation II";
					}
					if (this.pixellationNation < 4 && this.checkBoard()) {
						this.pixellationNation += 4;
						this.AP += 1000;
						this.changeTotalAP(1000);
						this.message = "Pixellation Nation III";
					}
				}).bind(this, this.pixel.x, this.pixel.y))
				.catch(error => console.log(error));
			this.sneakPeeks.unshift({
				x: this.pixel.x,
				y: this.pixel.y,
				value: this.pixel.numLocks * COLORS.length + nextColor
			});
			this.painter.changePixel(this.pixel.x, this.pixel.y, nextColor);
			this.race.waiting = true;
		},

		sendTalk: function () {
			if (this.newChat.message.trim().length === 0
							|| this.AP < COST_TALK) return;
			fetch(CHAT_URL, {
				method: "POST",
				headers: {"Content-Type": "application/json"},
				body: JSON.stringify({
					n: this.newChat.nick.trim().substring(0, MAX_NICK),
					m: this.newChat.message.trim().substring(0, MAX_MSG)
				})
			})
				.then(this.loadChats)
				.catch(error => console.log(
					`POST chat/ error: ${JSON.stringify(error)}`
				));
			this.spendAP(COST_TALK);
			this.messagesSent += 1;
			if (this.messagesSent >= 2500 && this.chatterbox < 5) {
				this.chatterbox = 5;
				this.AP += 250;
				this.changeTotalAP(250);
				this.message = "Chatterbox V";
			}
			else if (this.messagesSent >= 1000 && this.chatterbox < 4) {
				this.chatterbox = 4;
				this.AP += 100;
				this.changeTotalAP(100);
				this.message = "Chatterbox IV";
			}
			else if (this.messagesSent >= 500 && this.chatterbox < 3) {
				this.chatterbox = 3;
				this.AP += 50;
				this.changeTotalAP(50);
				this.message = "Chatterbox III";
			}
			else if (this.messagesSent >= 100 && this.chatterbox < 2) {
				this.chatterbox = 2;
				this.AP += 10;
				this.changeTotalAP(10);
				this.message = "Chatterbox II";
			}
			else if (this.messagesSent >= 10 && this.chatterbox < 1) {
				this.chatterbox = 1;
				this.AP += 1;
				this.changeTotalAP(1);
				this.message = "Chatterbox I";
			}

			this.newChat.message = "";
		},

		changeTotalAP(value) {
			this.totalAP += value;
			if (this.totalAP >= 10000 && this.APAce < 5) {
				this.APAce = 5;
				this.AP += 250;
				this.changeTotalAP(250);
				this.message = "AP Ace V";
			}
			else if (this.totalAP >= 5000 && this.APAce < 4) {
				this.APAce = 4;
				this.AP += 100;
				this.changeTotalAP(100);
				this.message = "AP Ace IV";
			}
			else if (this.totalAP >= 2500 && this.APAce < 3) {
				this.APAce = 3;
				this.AP += 50;
				this.changeTotalAP(50);
				this.message = "AP Ace III";
			}
			else if (this.totalAP >= 500 && this.APAce < 2) {
				this.APAce = 2;
				this.AP += 10;
				this.changeTotalAP(10);
				this.message = "AP Ace II";
			}
			else if (this.totalAP >= 10 && this.APAce < 1) {
				this.APAce = 1;
				this.AP += 1;
				this.changeTotalAP(1);
				this.message = "AP Ace I";
			}
		},

		checkRow(row){
			let color = this.board[0][row] % COLORS.length;
			for (let i = 1; i < WIDTH; i++) {
				if (this.board[i][row] % COLORS.length !== color) {
					return false;
				}
			}
			return true;
		},

		checkCol(col) {
			let color = this.board[col][0] % COLORS.length;
			for (let i = 1; i < HEIGHT; i++) {
				if (this.board[col][i] % COLORS.length !== color) {
					return false;
				}
			}
			return true;
		},

		checkBoard(){
			let color = this.board[0][0] % COLORS.length;
			for (let x = 0; x < WIDTH; x++) {
				for (let y = 0; y < HEIGHT; y++) {
					if (this.board[x][y] % COLORS.length !== color) {
						return false;
					}
				}
			}
			return true;
		},

		spendAP(val){
			this.APSpent += val;
			if (this.APSpent >= 10000 && this.spendCrazy < 5) {
				this.spendCrazy = 5;
				this.AP += 250;
				this.changeTotalAP(250);
				this.message = "Spend Crazy V";
			}
			else if (this.APSpent >= 5000 && this.spendCrazy < 4) {
				this.spendCrazy = 4;
				this.AP += 100;
				this.changeTotalAP(100);
				this.message = "Spend Crazy IV";
			}
			else if (this.APSpent >= 2500 && this.spendCrazy < 3) {
				this.spendCrazy = 3;
				this.AP += 50;
				this.changeTotalAP(50);
				this.message = "Spend Crazy III";
			}
			else if (this.APSpent >= 500 && this.spendCrazy < 2) {
				this.spendCrazy = 2;
				this.AP += 10;
				this.changeTotalAP(10);
				this.message = "Spend Crazy II";
			}
			else if (this.APSpent >= 10 && this.spendCrazy < 1) {
				this.spendCrazy = 1;
				this.AP += 1;
				this.changeTotalAP(1);
				this.message = "Spend Crazy I";
			}
		},
		showAchievement(){
			$("#achievementUnlocked").css("right", 20);
			setTimeout(this.hideAchievement, 3000);
		},
		hideAchievement(){
			$("#achievementUnlocked").css("right", -250);
		}
	},

	mounted() {
		this.painter = new Painter(() => {
			this.selectXY.x = this.painter.selectX;
			this.selectXY.y = this.painter.selectY;
		});
		this.getMessage();
		this.getChats();
		this.getAP();
		this.getAchievementCounters();
		this.getAchievements();
		this.addAP();
		setInterval(this.getMessage, 3000);
		setInterval(this.getChats, 5000);
	},

	watch: {
		AP(newAP) {
			localStorage.AP = newAP;
		},
		totalAP(total) {
			localStorage.totalAP = total;
		},
		APSpent(total) {
			localStorage.APSpent = total;
		},
		colorsChanged(total) {
			localStorage.colorsChanged = total;
		},
		messagesSent(total) {
			localStorage.messagesSent = total;
		},
		colorCrazy(level) {
			localStorage.colorCrazy = level;
			$("#CC" + level.toString()).css("opacity", 1);
		},
		pixellationNation(level) {
			localStorage.pixellationNation = level;
			$("#PN" + level.toString()).css("opacity", 1);
		},
		spendCrazy(level) {
			localStorage.spendCrazy = level;
			$("#SC" + level.toString()).css("opacity", 1);
		},
		APAce(level) {
			localStorage.APAce = level;
			$("#AP" + level.toString()).css("opacity", 1);
		},
		chatterbox(level) {
			localStorage.chatterbox = level;
			$("#CB" + level.toString()).css("opacity", 1);
		},
		message(){
			this.showAchievement();
		}
	}
});
