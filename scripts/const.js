////////////////////////////////////////////////////////////////////////////////
// const.js                                                                   //
// Global constants, used across the application                              //
// Experts: Alexey                                                            //
////////////////////////////////////////////////////////////////////////////////



// URL to retrieve the board
const BOARD_URL = "board/";
// URL to retrieve chats
const CHAT_URL = "chat/";

// Alphabet for decoding tiles
const BASE_64_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
				+ "abcdefghijklmnopqrstuvwxyz" + "0123456789" + "-_";

// Sizes of board elements
// These values MUST match those on the backend. If there is a mismatch, the
// whole app will burn in fire and all puppies will be sad.
const WIDTH = 256;  // The size of the board, in pixels
const HEIGHT = 256;
const TILE_SIZE = 32;
const NUM_TILES = 8;
const HASH_LENGTH = 16;

// Possible colors for pixels on the board
// The length of this array is the total number of colors, which is also stored
// on the backend. Thus, if you want to change the number of colors here, you
// must change the backend accordingly. The colors themselves aren't stored on
// the backend, so you can freely change them here.
const COLORS = [
	{name: "Black",   css: "#000", red:   0, green:   0, blue:   0, alpha: 255},
	{name: "Blue",    css: "#00f", red:   0, green:   0, blue: 255, alpha: 255},
	{name: "Green",   css: "#0f0", red:   0, green: 255, blue:   0, alpha: 255},
	{name: "Cyan",    css: "#0ff", red:   0, green: 255, blue: 255, alpha: 255},
	{name: "Red",     css: "#f00", red: 255, green:   0, blue:   0, alpha: 255},
	{name: "Magenta", css: "#f0f", red: 255, green:   0, blue: 255, alpha: 255},
	{name: "Yellow",  css: "#ff0", red: 255, green: 255, blue:   0, alpha: 255},
	{name: "White",   css: "#fff", red: 255, green: 255, blue: 255, alpha: 255}
];

const SWATCHES = ['#000000', '#0000ff', '#00ff00', '#00ffff',
 '#ff0000', '#ff00ff', '#ffff00', '#ffffff'];

// AP costs of different actions
// If you change costs here, change the button labels in the HTML accordingly.
const COST_LOCK = 3;
const COST_UNLOCK = 3;
const COST_CHANGE = 1;
const COST_TALK = 1;

// Time to go from 0 AP to 1 AP, in milliseconds
const PROGRESS_BASE = 5000;
// The ratio by which the time to get each subsequent AP increases
const PROGRESS_RATIO = 1.5;

// Maximum number of characters in chat nick / message
const MAX_NICK = 32;
const MAX_MSG = 256;
