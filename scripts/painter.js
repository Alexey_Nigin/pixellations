////////////////////////////////////////////////////////////////////////////////
// painter.js                                                                 //
// A class to render the board and let the player visually interact with it   //
// Experts: Alexey                                                            //
// Technical debt: We assumed that the board is always at the top-left of the //
// screen, things will break if it gets moved.                                //
////////////////////////////////////////////////////////////////////////////////



// Painter-specific constants

// Zoom limits
// MAX_ZOOM / MIN_ZOOM must be a power of 2.
const MIN_ZOOM = 1;
const MAX_ZOOM = 32;
// The constant used to distinguish clicking from dragging.
// If it's hard to drag the canvas and it gets clicked instead, decrease the
// value of MIN_DRAG. If it's hard to click the canvas and it gets dragged
// instead, increase the value of MIN_DRAG.
const MIN_DRAG = 10;



class Painter {
	// Constructor
	// Create one instance only and never destroy it.
	// callback is a function called when the user selects a pixel.
	constructor(callback) {
		// Save the callback function
		this._callback = callback;
		// The visible element where the board is displayed
		this._elBoard = document.getElementById("board");
		// The invisible element where the board is pre-rendered
		this._elRenderer = document.createElement("canvas");
		this._elRenderer.width = WIDTH;
		this._elRenderer.height = HEIGHT;
		// Location of the center of the board, in pixels
		// This is relative to the center of the display (this._elBoard). E.g.
		// if this._centerX === 100, the board is shifted 100px to the right
		// from the center of the display.
		this._centerX = 0;
		this._centerY = 0;
		// Zoom level, in screen pixels / board pixel
		this._zoom = MIN_ZOOM;
		// Is the player currently attempting to click or drag?
		this._dragging = false;
		// Where did the dragging start?
		// Used to distinguish clicking from dragging.
		this._dragStartX = NaN;
		this._dragStartY = NaN;
		// Where was the mouse at the previous moment?
		// Used to calculate dragging distance.
		this._dragPrevX = NaN;
		this._dragPrevY = NaN;
		// True if the player dragged, false if the player clicked
		this._dragSuccessful = false;
		// Coordinates of the selected pixel
		this.selectX = 0;
		this.selectY = 0;
		// Array of pixels that have recently changed
		this._changes = [];
		// Whether the canvas needs to re-render
		this._needRender = false;

		// Event listeners
		this._elBoard.addEventListener("wheel", this._onWheel.bind(this));
		this._elBoard.addEventListener("mousedown", this._onDown.bind(this));
		this._elBoard.addEventListener("mousemove", this._onMove.bind(this));
		this._elBoard.addEventListener("mouseup", this._onUp.bind(this));
		this._elBoard.addEventListener("mouseout", this._onUp.bind(this));

		// Zoom buttons
		let elZoomIn = document.getElementById("zoomIn");
		let elZoomOut = document.getElementById("zoomOut");
		elZoomIn.addEventListener("click", this._zoomIn.bind(this, 0, 0));
		elZoomOut.addEventListener("click", this._zoomOut.bind(this, 0, 0));

		// Start repaint loop
		this._repaint();
	}

	// Load the board data
	// board is a WIDTH x HEIGHT 2D array of single-int lock/color data. changes
	// is an array of coordinates of pixels that have changed; every element of
	// changes is an object with properties x and y
	loadBoard(board, changes) {
		let ctx = this._elRenderer.getContext("2d");
		let imgData = ctx.createImageData(WIDTH, HEIGHT);
		for (let x = 0; x < WIDTH; ++x) {
			for (let y = 0; y < HEIGHT; ++y) {
				let color = COLORS[board[x][y] % COLORS.length];
				let startIndex = 4 * (WIDTH * y + x);
				imgData.data[startIndex + 0] = color.red;
				imgData.data[startIndex + 1] = color.green;
				imgData.data[startIndex + 2] = color.blue;
				imgData.data[startIndex + 3] = color.alpha;
			}
		}
		ctx.putImageData(imgData, 0, 0);

		for (let c of changes) {
			this._changes.push({x: c.x, y: c.y, age: 0});
		}
		this._needRender = true;
	}

	// Change a single pixel
	changePixel(x, y, color) {
		let ctx = this._elRenderer.getContext("2d");
		let imgData = ctx.createImageData(1, 1);
		imgData.data[0] = COLORS[color].red;
		imgData.data[1] = COLORS[color].green;
		imgData.data[2] = COLORS[color].blue;
		imgData.data[3] = COLORS[color].alpha;
		ctx.putImageData(imgData, x, y);
		this._needRender = true;
	}

	// Repaint the board onto the display
	// Internally called once, then it calls itself in a loop.
	_repaint() {
		// Check if we can be lazy and avoid doing the work
		if (
			!this._needRender
			&& this._changes.length === 0
			&& this._elBoard.width === this._elBoard.clientWidth
			&& this._elBoard.height === this._elBoard.clientHeight
		) {
			window.requestAnimationFrame(this._repaint.bind(this));
			return;
		}

		// Make sure the two sizes of this._elBoard match
		// It's really stupid that we have to do it manually.
		this._elBoard.width = this._elBoard.clientWidth;
		this._elBoard.height = this._elBoard.clientHeight;

		// Draw the board
		let ctx = this._elBoard.getContext("2d");
		// Ensure pixellated look
		ctx.imageSmoothingEnabled = false;
		ctx.drawImage(
			this._elRenderer,
			this._toDisplayX(0),
			this._toDisplayY(0),
			this._zoom * WIDTH,
			this._zoom * HEIGHT
		);

		// Draw the selection rectangle
		if (this._zoom > 2) {
			ctx.lineWidth = (this._zoom === 4) ? 1 : 2;
			ctx.strokeStyle = "#000";
			ctx.strokeRect(
				this._toDisplayX(this.selectX) + ctx.lineWidth / 2,
				this._toDisplayY(this.selectY) + ctx.lineWidth / 2,
				this._zoom - ctx.lineWidth,
				this._zoom - ctx.lineWidth
			);
			ctx.strokeStyle = "#fff";
			ctx.strokeRect(
				this._toDisplayX(this.selectX) + 3 * ctx.lineWidth / 2,
				this._toDisplayY(this.selectY) + 3 * ctx.lineWidth / 2,
				this._zoom - 3 * ctx.lineWidth,
				this._zoom - 3 * ctx.lineWidth
			);
		}

		// Draw circles around changed pixels
		ctx.lineWidth = 2;
		for (let c of this._changes) {
			ctx.beginPath();
			ctx.strokeStyle = "rgba(0, 0, 0, "
							+ (100 - c.age * c.age / 10000) + "%)";
			ctx.arc(
				this._toDisplayX(c.x + 0.5),
				this._toDisplayY(c.y + 0.5),
				c.age / 30,
				0,
				2 * Math.PI
			);
			ctx.stroke();
			ctx.beginPath();
			ctx.strokeStyle = "rgba(255, 255, 255, "
							+ (100 - c.age * c.age / 10000) + "%)";
			ctx.arc(
				this._toDisplayX(c.x + 0.5),
				this._toDisplayY(c.y + 0.5),
				Math.max(c.age / 30 - 2, 0),
				0,
				2 * Math.PI
			);
			ctx.stroke();
			++c.age;
		}
		this._changes = this._changes.filter(c => c.age < 1000);

		// Make a note that everything is now rendered
		this._needRender = false;

		// Schedule next repaint
		window.requestAnimationFrame(this._repaint.bind(this));
	}

	// Zoom the board out
	// x and y are how far the center of the zoom is from the center of the
	// display.
	_zoomOut(x, y) {
		if (this._zoom > MIN_ZOOM) {
			this._zoom /= 2;
			this._centerX = (this._centerX - x) / 2;
			this._centerY = (this._centerY - y) / 2;
			this._needRender = true;
		}
	}

	// Zoom the board in
	// x and y are how far the center of the zoom is from the center of the
	// display.
	_zoomIn(x, y) {
		if (this._zoom < MAX_ZOOM){
			this._zoom *= 2;
			this._centerX = 2 * this._centerX + x;
			this._centerY = 2 * this._centerY + y;
			this._needRender = true;
		}
	}

	// Handle onwheel
	_onWheel(event) {
		// See technical debt at the top
		let x = this._elBoard.width / 2 - event.pageX;
		let y = this._elBoard.height / 2 - event.pageY;
		// event.deltaY is positive if down, negative if up
		if (event.deltaY > 0) {
			this._zoomOut(x, y);
		} else {
			this._zoomIn(x, y);
		}
	}

	// Handle onmousedown
	// Just sets up all variables.
	_onDown(event) {
		this._dragging = true;
		this._dragStartX = event.pageX;
		this._dragStartY = event.pageY;
		this._dragPrevX = event.pageX;
		this._dragPrevY = event.pageY;
		this._dragSuccessful = false;
	}

	// Handle onmousemove
	_onMove(event) {
		// Nothing to do if the mouse is up
		if (!this._dragging) return;

		// If the mouse is MIN_DRAG away from dragStart, the player is dragging
		// (As opposed to clicking)
		if (Math.hypot(event.pageX - this._dragStartX,
						event.pageY - this._dragStartY) > MIN_DRAG) {
			this._dragSuccessful = true;
		}

		// If the user is dragging, calculate the drag
		if (this._dragSuccessful) {
			// See technical debt at the top
			this._centerX += event.pageX - this._dragPrevX;
			this._centerY += event.pageY - this._dragPrevY;
			this._dragPrevX = event.pageX;
			this._dragPrevY = event.pageY;
			this._needRender = true;
		}
	}

	// Handle onmouseup
	// Resets all variables, triggers click action if the player clicked.
	_onUp(event) {
		// Nothing to do if the mouse is up
		if (!this._dragging) return;

		this._dragging = false;
		if (!this._dragSuccessful) {
			// See technical debt at the top
			this.selectX = this._toBoardX(event.pageX);
			this.selectY = this._toBoardY(event.pageY);
			this._callback();
			this._needRender = true;
		}
		this._dragStartX = NaN;
		this._dragStartY = NaN;
		this._dragPrevX = NaN;
		this._dragPrevY = NaN;
		this._dragSuccessful = false;
	}

	// Convert board x to display x
	_toDisplayX(x) {
		return Math.round((this._elBoard.width - this._zoom * WIDTH) / 2
						+ this._centerX + this._zoom * x);
	}

	// Convert board y to display y
	_toDisplayY(y) {
		return Math.round((this._elBoard.height - this._zoom * HEIGHT) / 2
						+ this._centerY + this._zoom * y);
	}

	// Convert display x to board x
	_toBoardX(x) {
		let raw = Math.floor((x - this._centerX - this._elBoard.width / 2)
						/ this._zoom + WIDTH / 2);
		return Math.min(Math.max(raw, 0), WIDTH - 1);
	}

	// Convert display y to board y
	_toBoardY(y) {
		let raw = Math.floor((y - this._centerY - this._elBoard.height / 2)
						/ this._zoom + HEIGHT / 2);
		return Math.min(Math.max(raw, 0), HEIGHT - 1);
	}
}
