# Pixellations API specification

[Official implementation](https://gitlab.com/Alexey_Nigin/nigin-rocks/-/blob/master/api/pixellations.py).

Upon any incorrect usage, the server should return 4xx with a human-readable
error message.

## `board/`

The board endpoints may appear complicated at fist glance. That is because they
are optimized to require as little bandwidth as possible, under the assumptions
that the board doesn't change much and when it does the changes are usually
localized.

### Terminology

#### Board and tiles

The 256x256 place where people draw things is called the board. The board is
divided into 64 tiles of size 32x32, like a chessboard.

#### Zipping pixels

Each pixel on the board has a `color` (0-7) and a `numLocks` (0-255). To
efficiently send information about a pixel over the network, we zip the pixel.

Zipping a pixel goes as follows:

1. We combined `color` and `numLocks` into `singleNumber`:
   `singleNumber = 8 * numLocks + color`
2. We define a `BASE_64_ALPHABET` as follows:
   `BASE_64_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"`.
   We assume `BASE_64_ALPHABET` is 0-indexed.
3. If `singleNumber < 32`, the zipped representation of the pixel is one
   character: `pixelZip = BASE_64_ALPHABET[singleNumber]`
4. If `singleNumber >= 32`, the zipped representation of the pixel is two
   characters: `pixelZip = BASE_64_ALPHABET[32 + floor(singleNumber/64)] + BASE_64_ALPHABET[singleNumber%64]`

This representation, inspired by UTF-8, ensures that pixels can be unambiguously
unzipped even if their zipped representations are concatenated as strings.

#### Serialization

Whenever we deal with 2D data, we serialize it as follows: top row left to
right, then next row left to right, and so on. This is the opposite order from
what may be natural to a coder writing `for` loops, but it is more readable.

We can serialize data either into JSON arrays or into a single string, we will
call these approaches "serialize into an array" and "serialize into a string"
respectively.

#### Zipping tiles

To zip a tile, we zip each individual pixel of that tile and then serialize the
zipped pixels into a string.

#### Tile hashes

Tile hashes are 16-character long hashes of tile zips, provided by the server.
The exact implementation of the hash is up to you, as long as it's always the
same for the same tile zip, and almost always different for different tile zips.

Tile hashes shouldn't use any weird special characters.

### `GET board/`

#### Request

Plain `GET` request, for an optional `Tile-Hashes` header, the value of which is
the hashes of all tiles serialized into a string. Hashes can be obtained from
previous responses, and are used to avoid re-sendng redundant data.

#### Response

An array with 64 elements, which gives data about each tile serialized into an
array.

Each element of the array can be `null`, if based on the `Tile-Hashes` in the
request the server thinks that the client already has the latest version of this
tile.

If the element is not `null`, then it is a single string - the first 16
characters of this string are the new tile hash, and the rest of the string is
the zipped tile.

### `PATCH board/`

#### Request

Same `Tile-Hashes` header as in `GET board/`.

Additionally, any of the following JSON bodies:

```json
{
	"x": 1,
	"y": 2,
	"currColor": 3,
	"action": "LOCK"
}
```

```json
{
	"x": 1,
	"y": 2,
	"currColor": 3,
	"action": "UNLOCK"
}
```

```json
{
	"x": 1,
	"y": 2,
	"currColor": 3,
	"action": "CHANGE",
	"nextColor": 4
}
```

`currColor` is the color of the pixel to the current best knowledge of the
client, and is used by the server to resolve race conditions.

#### Response

```json
{
	"success": true,
	"tiles": [...]
}
```

`success` is true iff the server was able to perform the requested operation.

`tiles` has the same format as the entire response from `GET board/`, with the
new board state.

## `chat/`

### `GET chat/`

#### Request

Just a plain GET request, nothing special.

#### Response

```json
[
	{
		"n": "Nick 1",
		"m": "Message 1"
	},
	{
		"n": "Nick 2",
		"m": "Message 2"
	}
]
```

It is up to the server to decide how many latest chats to return, and whether to
back up older chats elsewhere.

### `POST chat/`

#### Request

JSON body:

```json
{
	"n": "New message nick",
	"m": "New message body"
}
```

Nick length limit: 32 characters.

Message length limit: 256 characters.

#### Response

Same as `GET chat/`, including the new message.
