# Pixellations

A collaborative pixel art drawing game!

## Credits

Pixellations was created by:

* Alexey Nigin
* Jonathan Liu
* Muhan Zhao
* Tsai-Ni (Ninnie) Chiang

This started out as a final project for
[EECS 493](https://bulletin.engin.umich.edu/courses/eecs/) at the University of
Michigan, and was inspired by [r/place](https://www.reddit.com/r/place/) and
[PixelLock](https://nigin.rocks/pixel/).

Pixellations is available under the
[Creative Commons Attribution-NonCommercial](https://creativecommons.org/licenses/by-nc/4.0/)
license.
